
.. image:: Assemblee_nationale_francaise_100X100.png

A quoi �a sert ?
================

Ce programme sert � r�cup�rer les adresses email des d�put�s de l'assembl�e nationale fran�aise.

Il permet de s�lectionner les adresses email � partir :

* du nom d'un parti,
* du nom d'une r�gion,
* du nom d'un d�partement.

Les donn�es sont r�cup�r�es � partir des donn�es du site de l'assembl�e nationnale.

Comment l'installer 
===================

1. En utilisant git : git clone https://https://github.com/Nicola-31/EmailDepute
2. En t�l�chargeant le zip 

Lien vers la doc
================

Voir le wiki_

.. _wiki: https://github.com/Nicola-31/EmailDepute/wiki/
